import { combineReducers } from "redux";
import Production from './production';

export default combineReducers({
    Production
});