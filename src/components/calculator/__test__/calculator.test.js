import { mount, shallow } from "enzyme/build";
import Calculator from "../calculator";


function setUp(){
    return(
        mount(<Calculator />)
    )
}
test('render whith out error component app',()=>{
    const wrapper = setUp();
    const componentCalcutor = wrapper.find('[data-test="parentComponent"]');
    expect(componentCalcutor.length).toBe(1);
});
