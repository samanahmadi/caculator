import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { additionProduction, allClear, dividerProduction, multiplierProduction, subString, subtractorProduction } from "../../actions/actionProdation/actionProduction";

const Calculator = () => {

    const production = useSelector((state) => state);
    const dispatch = useDispatch();

    const [operator, setOperator] = useState();
    const [collectNumbersArray, setCollectNumbersArray] = useState([]);
    const [collectNumbers, setCollectNumbers] = useState([]);
    const [modeChanger, setModeChanger] = useState(true);

    function collectNumber(number) {
        let authorization = [...collectNumbersArray, number].join('');
        if(authorization === "."){
            console.log(authorization)
            setCollectNumbersArray([...collectNumbersArray, number].join(''));
            if (collectNumbersArray.length > 9) {
                document.getElementsByClassName("calculator-screen")[0].style.fontSize = '2rem';
            }
            if (collectNumbersArray.length < 9) {
                document.getElementsByClassName("calculator-screen")[0].style.fontSize = '3rem';
            }
        }else{
            if (authorization * authorization != 0 ) {
                setCollectNumbersArray([...collectNumbersArray, number].join(''));
                if (collectNumbersArray.length > 9) {
                    document.getElementsByClassName("calculator-screen")[0].style.fontSize = '2rem';
                }
                if (collectNumbersArray.length < 9) {
                    document.getElementsByClassName("calculator-screen")[0].style.fontSize = '3rem';
                }
            }
        }
        
    }
    function collectNumberSeccond(number) {
        let authorization = [...collectNumbers, number].join('');
        if (authorization * authorization != 0 ) {
            setCollectNumbers([...collectNumbers, number].join(''));
            if (collectNumbers.length > 9) {
                document.getElementsByClassName("calculator-screen")[0].style.fontSize = '2rem';
            }
            if (collectNumbers.length < 9) {
                document.getElementsByClassName("calculator-screen")[0].style.fontSize = '3rem';
            }
        }
    }
    function resetCollectNumber() {
        setCollectNumbersArray([]);
        setCollectNumbers([]);
        setOperator(' ');
    }
    function showResult() {
        setCollectNumbersArray([]);
        setCollectNumbers([]);
        setOperator(' ');
        setModeChanger(true);
        switch (operator) {
            case "+":
                return dispatch(additionProduction(collectNumbersArray, collectNumbers));
            case "-":
                return dispatch(subtractorProduction(collectNumbersArray, collectNumbers));
            case "*":
                return dispatch(multiplierProduction(collectNumbersArray, collectNumbers));
            case "/":
                return dispatch(dividerProduction(collectNumbersArray, collectNumbers));
            default:
                return null
        }
    }
    function clearEntry() {
        production.Production ? (dispatch(subString(production.Production))) :
            (modeChanger === true ? setCollectNumbersArray(collectNumbersArray.substring(0, collectNumbersArray.length - 1)) :
                setCollectNumbers(collectNumbers.substring(0, collectNumbers.length - 1)))
    }
    function handlerKeyBoard(e) {
        switch (e.key) {
            case "1":
                return production.Production ? dispatch(allClear()) && modeChanger === true ? collectNumber(1) : collectNumberSeccond(1) : modeChanger === true ? collectNumber(1) : collectNumberSeccond(1)
            case "2":
                return production.Production ? dispatch(allClear()) && modeChanger === true ? collectNumber(2) : collectNumberSeccond(2) : modeChanger === true ? collectNumber(2) : collectNumberSeccond(2)
            case "3":
                return production.Production ? dispatch(allClear()) && modeChanger === true ? collectNumber(3) : collectNumberSeccond(3) : modeChanger === true ? collectNumber(3) : collectNumberSeccond(3)
            case "4":
                return production.Production ? dispatch(allClear()) && modeChanger === true ? collectNumber(4) : collectNumberSeccond(4) : modeChanger === true ? collectNumber(4) : collectNumberSeccond(4)
            case "5":
                return production.Production ? dispatch(allClear()) && modeChanger === true ? collectNumber(5) : collectNumberSeccond(5) : modeChanger === true ? collectNumber(5) : collectNumberSeccond(5)
            case "6":
                return production.Production ? dispatch(allClear()) && modeChanger === true ? collectNumber(6) : collectNumberSeccond(6) : modeChanger === true ? collectNumber(6) : collectNumberSeccond(6)
            case "7":
                return production.Production ? dispatch(allClear()) && modeChanger === true ? collectNumber(7) : collectNumberSeccond(7) : modeChanger === true ? collectNumber(7) : collectNumberSeccond(7)
            case "8":
                return production.Production ? dispatch(allClear()) && modeChanger === true ? collectNumber(8) : collectNumberSeccond(8) : modeChanger === true ? collectNumber(8) : collectNumberSeccond(8)
            case "9":
                return production.Production ? dispatch(allClear()) && modeChanger === true ? collectNumber(9) : collectNumberSeccond(9) : modeChanger === true ? collectNumber(9) : collectNumberSeccond(9)
            case "0":
                return production.Production ? dispatch(allClear()) && modeChanger === true ? collectNumber(0) : collectNumberSeccond(0) : modeChanger === true ? collectNumber(0) : collectNumberSeccond(0)
            case ".":
                return modeChanger === true ? collectNumber(".") : collectNumberSeccond(".")

            case "Backspace":
                return clearEntry();
            case "Enter":
                return showResult();
            case "Delete":
                return dispatch(allClear()), resetCollectNumber();
        }

        if (e.key === "+") {
            setOperator("+")
            setModeChanger(modeChanger === true ? false : true)
            if (production.Production) {
                modeChanger === true ? collectNumber(production.Production) : collectNumberSeccond(production.Production)
            }
        }
        if (e.key === "-") {
            setOperator("-")
            setModeChanger(modeChanger === true ? false : true)
            if (production.Production) {
                modeChanger === true ? collectNumber(production.Production) : collectNumberSeccond(production.Production)
            }
        }
        
        if (e.key === "*") {
            setOperator("*")
            setModeChanger(modeChanger === true ? false : true)
            if (production.Production) {
                modeChanger === true ? collectNumber(production.Production) : collectNumberSeccond(production.Production)
            }
        }
        if (e.key === "/") {
            setOperator("/")
            setModeChanger(modeChanger === true ? false : true)
            if (production.Production) {
                modeChanger === true ? collectNumber(production.Production) : collectNumberSeccond(production.Production)
            }
        }
    }
    useEffect(()=>{
        document.getElementById("addEventListener").focus();
    },[]);
    
    return (
        <div className="calculator card" data-test="parentComponent" onKeyUp={(e) => {
            handlerKeyBoard(e)
        }}>
            <div className="calculator-screen z-depth-1">
                <div>
                    <p>{production.Production ? null : collectNumbersArray}{operator}{production.Production ? production.Production : collectNumbers}</p>
                </div>
            </div>

            <div className="calculator-keys">



                <button type="button" className="addition-sign operator btn btn-info" value="+" onClick={(e) => {
                    setOperator(e.target.value)
                    setModeChanger(modeChanger === true ? false : true)
                    if (production.Production) {
                        modeChanger === true ? collectNumber(production.Production) : collectNumberSeccond(production.Production)
                    }
                }}>+</button>
                <button type="button" className="subtract-sign operator btn btn-info" value="-" onClick={(e) => {
                    setOperator(e.target.value)
                    setModeChanger(modeChanger === true ? false : true)
                    if (production.Production) {
                        modeChanger === true ? collectNumber(production.Production) : collectNumberSeccond(production.Production)
                    }
                }}>-</button>
                <button type="button" className="multiple-sign operator btn btn-info" value="*" onClick={(e) => {
                    setOperator(e.target.value)
                    setModeChanger(modeChanger === true ? false : true)
                    if (production.Production) {
                        modeChanger === true ? collectNumber(production.Production) : collectNumberSeccond(production.Production)
                    }
                }}>&times;</button>
                <button type="button" className="divider-sign operator btn btn-info" value="/" onClick={(e) => {
                    setOperator(e.target.value)
                    setModeChanger(modeChanger === true ? false : true)
                    if (production.Production) {
                        modeChanger === true ? collectNumber(production.Production) : collectNumberSeccond(production.Production)
                    }
                }}>&divide;</button>



                <button type="button" value="7" className="button-7 btn btn-light waves-effect" onClick={(e) => {
                    production.Production ? dispatch(allClear()) && modeChanger === true ? collectNumber(e.target.value) : collectNumberSeccond(e.target.value) : modeChanger === true ? collectNumber(e.target.value) : collectNumberSeccond(e.target.value)
                }}>7</button>

                <button type="button" value="8" className="button-8 btn btn-light waves-effect" onClick={(e) => {
                    production.Production ? dispatch(allClear()) && modeChanger === true ? collectNumber(e.target.value) : collectNumberSeccond(e.target.value) : modeChanger === true ? collectNumber(e.target.value) : collectNumberSeccond(e.target.value)
                }}>8</button>

                <button type="button" value="9" className="button-9 btn btn-light waves-effect" onClick={(e) => {
                    production.Production ? dispatch(allClear()) && modeChanger === true ? collectNumber(e.target.value) : collectNumberSeccond(e.target.value) : modeChanger === true ? collectNumber(e.target.value) : collectNumberSeccond(e.target.value)
                }}>9</button>

                <button type="button" value="4" className="button-4 btn btn-light waves-effect" onClick={(e) => {
                    production.Production ? dispatch(allClear()) && modeChanger === true ? collectNumber(e.target.value) : collectNumberSeccond(e.target.value) : modeChanger === true ? collectNumber(e.target.value) : collectNumberSeccond(e.target.value)
                }}>4</button>

                <button type="button" value="5" className="button-5 btn btn-light waves-effect" onClick={(e) => {
                    production.Production ? dispatch(allClear()) && modeChanger === true ? collectNumber(e.target.value) : collectNumberSeccond(e.target.value) : modeChanger === true ? collectNumber(e.target.value) : collectNumberSeccond(e.target.value)
                }}>5</button>

                <button type="button" value="6" className="button-6 btn btn-light waves-effect" onClick={(e) => {
                    production.Production ? dispatch(allClear()) && modeChanger === true ? collectNumber(e.target.value) : collectNumberSeccond(e.target.value) : modeChanger === true ? collectNumber(e.target.value) : collectNumberSeccond(e.target.value)
                }}>6</button>

                <button type="button" value="1" className="button-1 btn btn-light waves-effect" onClick={(e) => {
                    production.Production ? dispatch(allClear()) && modeChanger === true ? collectNumber(e.target.value) : collectNumberSeccond(e.target.value) : modeChanger === true ? collectNumber(e.target.value) : collectNumberSeccond(e.target.value)
                }}>1</button>

                <button type="button" value="2" className="button-2 btn btn-light waves-effect" onClick={(e) => {
                    production.Production ? dispatch(allClear()) && modeChanger === true ? collectNumber(e.target.value) : collectNumberSeccond(e.target.value) : modeChanger === true ? collectNumber(e.target.value) : collectNumberSeccond(e.target.value)
                }}>2</button>

                <button type="button" value="3" className="button-3 btn btn-light waves-effect" onClick={(e) => {
                    production.Production ? dispatch(allClear()) && modeChanger === true ? collectNumber(e.target.value) : collectNumberSeccond(e.target.value) : modeChanger === true ? collectNumber(e.target.value) : collectNumberSeccond(e.target.value)
                }}>3</button>

                <button type="button" value="0" className="button-0 btn btn-light waves-effect" onClick={(e) => {
                    production.Production ? dispatch(allClear()) && modeChanger === true ? collectNumber(e.target.value) : collectNumberSeccond(e.target.value) : modeChanger === true ? collectNumber(e.target.value) : collectNumberSeccond(e.target.value)
                }}>0</button>




                <button type="button" className="desimal-sign btn btn-light" value="." onClick={(e) => {
                    modeChanger === true ? collectNumber(e.target.value) : collectNumberSeccond(e.target.value)
                }}>.</button>

                <button type="button" className="ac-sign all-clear btn btn-info btn-sm" value="all-clear" onClick={() => {
                    dispatch(allClear());
                    resetCollectNumber();
                }}>AC</button>

                <button type="button" value="<=" className="clearEntry-sign btn btn-info waves-effect" onClick={() => {
                    clearEntry();
                }}>&#8592;</button>


                <button id="addEventListener" type="button" className="equal-sign operator btn btn-danger" value="=" onClick={() => {
                    showResult();
                }}>=</button>

            </div>
        </div>
    )
}
export default Calculator;