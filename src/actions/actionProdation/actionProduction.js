export const additionProduction = (firstNumber, secondNumber) => {
    let result;
    
    result=(
        Number(firstNumber) + Number(secondNumber)
    )
    if (result) {
        return {
            type: "ADDITION",
            paload: result
        }
    }

}
export const subtractorProduction = (firstNumber, secondNumber) => {
    let result;
    result = (
        Number(firstNumber) - Number(secondNumber)
    )
    if (result || result === 0) {
        return {
            type: "SUBTRACTOR",
            paload: result
        }
    }

}
export const multiplierProduction = (firstNumber, secondNumber) => {
    let result;
    result = (
        Number(firstNumber) * Number(secondNumber)
    )
    if (result) {
        return {
            type: "MULTIPLIER",
            paload: result
        }
    }else{
        return {
            type: "MULTIPLIER",
            paload: 0
        }
    }

}
export const dividerProduction = (firstNumber, secondNumber) => {
    let result;
    if (Number(firstNumber) > Number(secondNumber)) {
        result = Number(firstNumber) / Number(secondNumber)
    } else {
        result = Number(secondNumber) / Number(firstNumber)
    }
    if (result) {
        return {
            type: "DIVIDER",
            paload: result
        }
    }

}
export const allClear = () => {
    return {
        type: "ALLCLEAR",
        paload: 0
    }
}
export const subString = (parameters) => {
    
    parameters = parameters.toString()
    parameters =  parameters.substring(0, parameters.length - 1);
    return{
        type:'SUBSTRING',
        paload: Number(parameters)
    }
}