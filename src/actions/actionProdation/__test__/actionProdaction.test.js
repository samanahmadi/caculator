import { additionProduction, dividerProduction, multiplierProduction, subtractorProduction } from "../actionProduction"

test('Correctness of the adder function',()=>{
    expect(additionProduction(2,5)).toEqual({"paload": 7,"type": "ADDITION",});
});
test('Correctness of the subtraction function',()=>{
    expect(subtractorProduction(7,8)).toEqual({"paload": -1,"type": "SUBTRACTOR"});
});
test('Correctness of the multiptier function',()=>{
    expect(multiplierProduction(3,8)).toEqual({"paload": 24,"type": "MULTIPLIER"});
});
test('Correctness of the divider function',()=>{
    expect(dividerProduction(8,4)).toEqual({"paload": 2,"type": "DIVIDER"});
});